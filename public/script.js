let maxPoints = 300;

let LN5 = Math.log(5);
function log5(num) {
	return Math.log(num) / LN5;
}

function debounce(fn, time = 10) {
	let timeout = null;
	return function(...args) {
		if (timeout != null)
			clearTimeout(timeout);
		timeout = setTimeout(() => fn.apply(null, args), time);
	}
}

class CanvasView {
	constructor(ctx) {
		this.ctx = ctx;
		this.dpr = 1;
		this.x = 0;
		this.y = 0;
		this.width = ctx.canvas.width;
		this.height = ctx.canvas.height;
	}
}

let colors = [
	"rgb(114,147,203)", "rgb(225,151,76)", "rgb(132,186,91)", "rgb(211,94,96)",
	"rgb(128,133,133)", "rgb(144,103,167)", "rgb(171,104,87)", "rgb(204,194,16)",
];
class ColorGen {
	constructor() {
		this.colorIdx = 0;
	}

	create() {
		let c = colors[this.colorIdx];
		this.colorIdx = (this.colorIdx + 1) % colors.length;
		return c;
	}
}

class GraphLine {
	constructor(name, color) {
		this.name = name;
		this.title = name.split("\x1b")[0]; // Ignore anything after an esc
		this.color = color;
		this.points = [];
		this.valEl = document.createElement("span");
		this.valEl.className = "value";
		this.valEl.innerText = 0;
	}

	update(xval, yval) {
		this.points.push([ xval, yval ]);
	}

	draw(cv, minx, maxx, miny, maxy) {
		if (this.points.length > 0)
			this.valEl.innerText = this.points[this.points.length - 1][1].toFixed(2);
		if (this.points.length < 2)
			return;

		cv.ctx.beginPath();

		let [ xval1, yval1 ] = this.points[0];
		let xfrac1 = (xval1 - minx) / (maxx - minx);
		let yfrac1 = 1 - (yval1 - miny) / (maxy - miny);
		cv.ctx.moveTo(xfrac1 * cv.width + cv.x, yfrac1 * cv.height + cv.y);

		let step = Math.ceil(this.points.length / maxPoints);

		for (let i = 1; i < this.points.length; i += step) {
			let [ xval, yval ] = this.points[i];
			let xfrac = (xval - minx) / (maxx - minx);
			let yfrac = 1 - (yval - miny) / (maxy - miny);
			cv.ctx.lineTo(xfrac * cv.width + cv.x, yfrac * cv.height + cv.y);
		}

		cv.ctx.lineWidth = 2 * cv.dpr;
		cv.ctx.strokeStyle = this.color;
		cv.ctx.stroke();
	}
}

class Graph {
	constructor(name, graphs) {
		this.name = name;
		this.lines = {};
		this.shownLines = [];
		this.colorGen = new ColorGen();
		this.isDate = null;
		this.graphs = graphs;
		this.filterDirty = false;

		window.addEventListener("resize", this.draw.bind(this));

		this.rangeStart = 0;
		this.rangeEnd = 1;
		this.minx = null;
		this.maxx = null;
		this.miny = 0;
		this.maxy = null;

		this.canvas = document.createElement("canvas");
		this.ctx = this.canvas.getContext("2d");
		this.dpr = window.devicePixelRatio || 1;

		this.sidebarTitlebarTitleElem = document.createElement("span");
		this.sidebarTitlebarTitleElem.innerText = name;

		this.sidebarRangeStartElem = document.createElement("input");
		this.sidebarRangeStartElem.value = "0";
		this.sidebarRangeStartElem.addEventListener("change", this.onRangeChange.bind(this));
		this.sidebarRangeEndElem = document.createElement("input");
		this.sidebarRangeEndElem.value = "1";
		this.sidebarRangeEndElem.addEventListener("change", this.onRangeChange.bind(this));

		this.sidebarTitlebarRangeElem = document.createElement("span");
		this.sidebarTitlebarRangeElem.className = "range";
		this.sidebarTitlebarRangeElem.appendChild(this.sidebarRangeStartElem);
		this.sidebarTitlebarRangeElem.appendChild(document.createTextNode("-"));
		this.sidebarTitlebarRangeElem.appendChild(this.sidebarRangeEndElem);

		this.sidebarTitlebarElem = document.createElement("div");
		this.sidebarTitlebarElem.className = "title";
		this.sidebarTitlebarElem.appendChild(this.sidebarTitlebarTitleElem);
		this.sidebarTitlebarElem.appendChild(this.sidebarTitlebarRangeElem);

		this.sidebarFilterElem = document.createElement("input");
		this.sidebarFilterElem.placeholder = "filter";
		this.sidebarFilterElem.className = "filter";
		this.sidebarFilterElem.type = "search";
		this.sidebarFilterElem.addEventListener("change", this.onFilterChange.bind(this));

		this.sidebarLinesElem = document.createElement("div");
		this.sidebarLinesElem.className = "lines";

		this.sidebarElem = document.createElement("div");
		this.sidebarElem.appendChild(this.sidebarTitlebarElem);
		this.sidebarElem.appendChild(this.sidebarFilterElem);
		this.sidebarElem.appendChild(this.sidebarLinesElem);
		this.sidebarElem.className = "sidebar";

		this.elem = document.createElement("div");
		this.elem.appendChild(this.sidebarElem);
		this.elem.appendChild(this.canvas)
		this.elem.className = "graph";
	}

	setRange(start, end) {
		this.sidebarRangeStartElem.value = start.toString();
		this.sidebarRangeEndElem.value = end.toString();
		this.onRangeChange();
	}

	onRangeChange(evt) {
		this.rangeStart = parseFloat(this.sidebarRangeStartElem.value);
		if (isNaN(this.rangeStart)) {
			this.rangeStart = 0;
			this.sidebarRangeStartElem.value = "0";
		}

		this.rangeEnd = parseFloat(this.sidebarRangeEndElem.value);
		if (isNaN(this.rangeEnd)) {
			this.rangeEnd = 1;
			this.sidebarRangeEndElem.value = "1";
		}

		// If we're changing due to user input,
		// update the other graphs' filters too
		if (evt != null) {
			for (let name in this.graphs.graphs) {
				let graph = this.graphs.graphs[name];
				if (graph == this)
					continue;
				graph.setRange(this.rangeStart, this.rangeEnd);
			}
		}

		this.draw();
	}

	setFilter(str) {
		this.sidebarFilterElem.value = str;
		this.onFilterChange();
	}

	onFilterChange(evt) {
		let filter = this.sidebarFilterElem.value;

		let rx;
		if (filter == "") {
			rx = /.*/;
		} else {
			try {
				rx = new RegExp(filter, "i");
			} catch (err) {
				alert("Invalid regex: "+err.message);
				return;
			}
		}

		// If we're changing due to user input,
		// update the other graphs' filters too
		if (evt != null) {
			for (let name in this.graphs.graphs) {
				let graph = this.graphs.graphs[name];
				if (graph == this)
					continue;
				graph.setFilter(filter);
			}
		}

		this.shownLines = [];
		this.sidebarLinesElem.innerHTML = "";

		for (let i in this.lines) {
			let line = this.lines[i];
			if (!rx.test(line.title)) continue;

			this.shownLines.push(line);

			let keyEl = document.createElement("span");
			keyEl.className = "key";
			keyEl.innerText = line.title;

			let el = document.createElement("div");
			el.className = "line";
			el.style.color = line.color;
			el.appendChild(keyEl);
			el.appendChild(line.valEl);
			this.sidebarLinesElem.appendChild(el);
		}

		this.draw();
	}

	getOrAddLine(name) {
		if (!this.lines[name]) {
			this.lines[name] = new GraphLine(name, this.colorGen.create());
			this.filterDirty = true;
		}
		return this.lines[name];
	}

	update(update) {
		let xval = update.XVal;
		if (this.isDate == null) {
			let d = Date.parse(xval);
			this.isDate = !isNaN(d);
		}

		if (this.isDate) {
			xval = Date.parse(xval);
		} else {
			xval = parseFloat(xval);
		}

		if (this.minx == null)
			this.minx = xval;
		if (this.maxx == null || this.maxx < xval)
			this.maxx = xval;

		for (let lineUpdate of update.Lines) {
			let yval = lineUpdate.YVal;
			let line = this.getOrAddLine(lineUpdate.Name);
			line.update(xval, yval);

			if (this.maxy == null || this.maxy < yval)
				this.maxy = yval;
		}
	}

	draw() {
		if (this.filterDirty) {
			this.filterDirty = false;
			this.onFilterChange();
		}

		this.canvas.width = this.canvas.clientWidth * this.dpr;
		this.canvas.height = this.canvas.clientHeight * this.dpr;
		this.ctx.font = (10 * this.dpr) + "px sans";
		this.ctx.lineWidth = 1 * this.dpr;
		this.ctx.strokeStyle = "#ccc";

		let pad = 6 * this.dpr;

		// The canvas view for lines
		let cv = new CanvasView(this.ctx);
		cv.dpr = this.dpr;
		cv.x += 40 * this.dpr;
		cv.width -= cv.x;
		cv.height -= 40 * this.dpr;

		let minx = this.minx;
		let maxx = this.maxx;
		let miny = this.miny;
		let maxy = this.maxy * 1.2;

		minx = (maxx - minx) * this.rangeStart + minx;
		maxx = (maxx - minx) * this.rangeEnd + minx;

		// Draw outline of graph
		{
			this.ctx.beginPath();
			this.ctx.moveTo(cv.x, 0);
			this.ctx.lineTo(cv.x, cv.height);
			this.ctx.lineTo(cv.x + cv.width, cv.height);
			this.ctx.stroke();
		}

		// Draw ytics
		this.ctx.textBaseline = "bottom";
		{
			let log = Math.floor(Math.log10(maxy - miny));
			let step = Math.pow(10, log);
			while ((maxy - miny) / step < 5)
				step /= 2;

			for (let i = miny; i < maxy; i += step) {
				let frac = 1 - (i - miny) / (maxy - miny);
				this.ctx.beginPath();
				this.ctx.moveTo(0, frac * cv.height + cv.y);
				this.ctx.lineTo(this.canvas.width, frac * cv.height + cv.y);
				this.ctx.stroke();

				this.ctx.beginPath();
				this.ctx.fillText(
					i.toString(),
					pad,
					frac * cv.height + cv.y - pad,
					cv.x);
			}
		}

		// Draw xtics
		this.ctx.textBaseline = "top";
		{
			let log = Math.floor(Math.log10(maxx - minx));
			let step = Math.pow(10, log);
			while ((maxx - minx) / step < 5)
				step /= 2;

			for (let i = minx; i < maxx; i += step) {
				let frac = (i - minx) / (maxx - minx);
				this.ctx.beginPath();
				this.ctx.moveTo(frac * cv.width + cv.x, 0);
				this.ctx.lineTo(frac * cv.width + cv.x, this.canvas.height);
				this.ctx.stroke();

				this.ctx.beginPath();
				let strs;
				if (this.isDate)
					strs = new Date(i).toISOString().split(".")[0].split("T");
				else
					strs = [ i.toString() ];

				for (let j = 0; j < strs.length; ++j) {
					this.ctx.fillText(
						strs[j],
						frac * cv.width + cv.x + pad,
						cv.height + cv.y + pad + 10 * this.dpr * j);
				}
			}
		}

		// Draw lines
		for (let line of this.shownLines) {
			line.draw(cv, minx, maxx, miny, maxy);
		}
	}
}

class GraphSet {
	constructor(elem) {
		this.graphs = {};
		this.elem = elem;
	}

	getOrAddGraph(name) {
		if (!this.graphs[name]) {
			this.graphs[name] = new Graph(name, this);
			this.elem.appendChild(this.graphs[name].elem);
		}
		return this.graphs[name];
	}

	draw() {
		for (let name in this.graphs) {
			this.graphs[name].draw();
		}
	}

	update(update) {
		for (let graphUpdate of update.Graphs) {
			this.getOrAddGraph(graphUpdate.Name).update(graphUpdate);
		}
	}

	reset() {
		this.graphs = {};
		this.elem.innerHTML = "";
	}
}

let graphs = new GraphSet(document.getElementById("graphs"));

function openWS() {
	let wsUrl =
		(location.protocol == "https:" ? "wss:" : "ws:")+
		"//"+location.host+"/listen";
	console.log("Opening "+wsUrl+"...");
	let ws = new WebSocket(wsUrl);
	ws.binaryType = "arraybuffer";
	window.ws = ws;
	let inflator = null;
	let initiated = false;

	let draw = debounce(() => graphs.draw());

	function createInflator() {
		inflator = new pako.Inflate();
		let linebuf = "";

		inflator.onData = function(chunk) {
			let str = linebuf + new TextDecoder().decode(chunk);

			let lines = str.split("\n");
			if (lines.length > 1)
				linebuf = "";

			for (let i = 0; i < lines.length - 1; ++i) {
				let obj;
				try {
					obj = JSON.parse(lines[i]);
				} catch (err) {
					console.log(err, lines[i]);
					continue;
				}
				graphs.update(obj);
			}

			linebuf += lines[lines.length - 1];

			draw();
		}

		inflator.onEnd = function(st) {
			if (st < 0) {
				console.log("Inflator error!", inflator.strm.msg);
			}
		}
	}

	ws.onopen = () => {
		inflator = null;
		graphs.reset();
		console.log("Opened");
	}

	ws.onerror = evt => {
		ws.close();
		console.log("Error", evt);
	}

	ws.onclose = evt => {
		console.log("Closed", evt);
		setTimeout(openWS, 2000);
	}

	let initseq = "===START===";
	let initbuf = [];
	for (let i = 0; i < initseq.length; ++i) {
		initbuf[i] = initseq.charCodeAt(i);
	}

	ws.onmessage = evt => {
		let data = new Uint8Array(evt.data);
		let init = true;
		for (let i = 0; i < initbuf.length; ++i) {
			if (data[i] != initbuf[i]) {
				init = false;
				break;
			}
		}

		if (init) {
			console.log("Got init sequence, creating inflator");
			createInflator();
			data = data.slice(initbuf.length);
		}

		if (data.length == 0)
			return;

		if (inflator == null) {
			console.log("Inflator is null!");
		} else {
			inflator.push(data, 2); // Z_SYNC_FLUSH
		}
	}
}
openWS();
