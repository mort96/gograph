package main

import (
	"fmt"
	"os"
	"bufio"
	"strings"
	"strconv"
	"io"
	"github.com/chzyer/readline"
	"gitlab.com/mort96/gograph"
)

func parseLine(line string) (gograph.GraphSetUpdate, error) {
	var update gograph.GraphSetUpdate

	sets := strings.Split(line, "|")
	for i, set := range sets {
		set = strings.TrimSpace(set)
		if set == "" {
			continue
		}

		parts := strings.Split(set, ";")
		if len(parts) != 3 {
			fmt.Printf("Set %d: Expected 3 parts separated by ';'\n", i + 1)
			continue
		}

		graphUpdate := gograph.GraphUpdate{
			Name: strings.TrimSpace(parts[0]),
			XVal: strings.TrimSpace(parts[1]),
		}

		graphLines := strings.Split(parts[2], ",")
		for j, graphLine := range graphLines {
			graphLine = strings.TrimSpace(graphLine)
			if graphLine == "" {
				continue
			}

			lineParts := strings.Split(graphLine, "=")
			if len(lineParts) > 2 {
				fmt.Printf("Set %d: Line %d: Extra =s. Are you missing a comma?\n",
					i + 1, j + 1)
				continue
			} else if len(lineParts) < 2 {
				fmt.Printf("Set %d: Line %d: Missing =s.\n", i + 1, j + 1)
				continue
			}

			yval, err := strconv.ParseFloat(strings.TrimSpace(lineParts[1]), 64)
			if err != nil {
				fmt.Printf("Set %d: Line %d: Number format error: %v\n",
					i + 1, j + 1, err)
				continue
			}

			graphLineUpdate := gograph.GraphLineUpdate{
				Name: strings.TrimSpace(lineParts[0]),
				YVal: yval,
			}

			graphUpdate.Lines = append(graphUpdate.Lines, graphLineUpdate)
		}

		update.Graphs = append(update.Graphs, graphUpdate)
	}

	return update, nil
}

func isTTY(file *os.File) bool {
	info, err := file.Stat()
	if err != nil {
		panic(err)
	}

	return (info.Mode() & os.ModeCharDevice) != 0
}

func main() {
	graph, err := gograph.NewGoGraph(nil)
	if err != nil {
		panic(err)
	}
	defer graph.Close()

	var addr string
	if len(os.Args) == 1 {
		addr = ":8080"
	} else if len(os.Args) == 2 {
		addr = os.Args[1]
	} else {
		println("Usage: "+os.Args[0]+" [address]")
		os.Exit(1)
	}

	go graph.Run(addr)
	println("Listening on "+addr)

	if (isTTY(os.Stdin)) {
		rl, err := readline.New("> ")
		if err != nil {
			panic(err)
		}
		defer rl.Close()

		for {
			line, err := rl.Readline()
			if err != nil && (err.Error() == "EOF" || err.Error() == "Interrupt") {
				break
			} else if err != nil {
				panic(err)
			}

			update, err := parseLine(string(line))
			if err != nil {
				fmt.Println(err)
				continue
			}

			err = graph.Update(update)
			if err != nil {
				fmt.Println(err)
				continue
			}
		}
	} else {
		reader := bufio.NewReader(os.Stdin)

		for {
			line, err := reader.ReadSlice('\n')
			if err != nil {
				if err == io.EOF {
					break
				}
				panic(err)
			}

			update, err := parseLine(string(line))
			if err != nil {
				fmt.Println(err)
				continue
			}

			err = graph.Update(update)
			if err != nil {
				fmt.Println(err)
				continue
			}
		}

		fmt.Println("stdin closed.")
		select{}
	}

}
