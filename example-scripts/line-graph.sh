#!/bin/sh

val1=100
val2=50
while :; do
	t="$(date --iso-8601=seconds)"
	val1=$(($val1 + ($RANDOM % 15 - 5)))
	val2=$(($val2 + ($RANDOM % 15 - 7)))
	echo "graph; $t; val 1 = $val1, val 2 = $val2"
	sleep 1
done
