#!/bin/sh

while :; do
	t="$(date --iso-8601=seconds)"
	lines="$(sensors | grep -E '+\d*.\d*°C' | grep -v 'N/A'  | sed 's/°C.*//; s/:\s*+\?/=/' | tr '\n' ',')"
	echo "graph; $t; $lines"
	sleep 1
done
