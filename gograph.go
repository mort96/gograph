package gograph

//go:generate statik -f -src public

import (
	"net/http"
	"encoding/json"
	"compress/gzip"
	"io/ioutil"
	"fmt"
	"sync"
	"os"
	"bytes"
	"github.com/gorilla/websocket"
	"github.com/rakyll/statik/fs"
	_ "gitlab.com/mort96/gograph/statik"
)

const bufsiz = 4096;

type GraphLineUpdate struct {
	Name string
	YVal float64
}

type GraphUpdate struct {
	Name string
	XVal string
	Lines []GraphLineUpdate
}

type GraphSetUpdate struct {
	Graphs []GraphUpdate
}

type GoGraph struct {
	listeners []*websocket.Conn
	wsmutex sync.Mutex
	log *os.File
	logIsTemp bool
	zipper *gzip.Writer
	zipbuf bytes.Buffer
}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool { return true },
}

func NewGoGraph(log *os.File) (*GoGraph, error) {
	graph := new(GoGraph)

	if log == nil {
		graph.logIsTemp = true
		log, err := ioutil.TempFile(os.TempDir(), "gograph-")
		if err != nil {
			return nil, err
		}
		graph.log = log
	} else {
		graph.log = log
	}

	return graph, nil
}

func (graph *GoGraph) Close() {
	if graph.logIsTemp {
		os.Remove(graph.log.Name())
	}
}

func (graph *GoGraph) Serve(addr string) {
	statikFS, err := fs.New();
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/listen", func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			fmt.Println(err)
			return
		}

		graph.wsmutex.Lock()
		defer graph.wsmutex.Unlock()

		graph.listeners = append(graph.listeners, conn)

		if graph.log != nil {
			_, err := graph.log.Seek(0, 0)
			if err != nil {
				fmt.Println(err)
				return
			}

			bytes := make([]byte, bufsiz)
			for {
				n, err := graph.log.Read(bytes)

				// Find the last 0xFF, which is the chunk terminator
				if n == bufsiz {
					for i := n - 1; i >= 0; i-- {
						if bytes[i] == 0xFF {
							n = i
							break
						}
					}
				}

				conn.WriteMessage(websocket.BinaryMessage, bytes[:n])

				if err != nil && err.Error() != "EOF" {
					fmt.Println(err)
					break
				} else if n == 0 {
					break
				}
			}

			graph.log.Seek(0, 2)
		}
	})

	http.Handle("/", http.FileServer(statikFS))

	err = http.ListenAndServe(addr, nil)
	if err != nil { panic(err) }
}

func (graph *GoGraph) Run(addr string) {
	graph.zipper = gzip.NewWriter(&graph.zipbuf)

	// Pad the existing file to be a multiple of bufsiz,
	// then write the start sequence.
	if graph.log != nil {
		size, err := graph.log.Seek(0, 2)
		if err != nil {
			panic(err)
		}

		pad := bufsiz - (size % bufsiz)
		if (size != 0 && pad > 0) {
			bytes := make([]byte, pad)
			bytes[0] = 0xFF // Chunk terminator
			fmt.Printf("File is %d bytes, padding to %d bytes to reach chunk boundary\n",
				size, size +pad)
			n, err := graph.log.Write(bytes)
			if err != nil {
				panic(err)
			} else if int64(n) != pad {
				fmt.Printf("Expected to pad %d bytes, but only wrote %d bytes.\n", pad, n)
				panic("Failed to pad.")
			}
		}

		_, err = graph.log.Write([]byte("===START==="))
		if err != nil {
			panic(err)
		}
	}

	graph.Serve(addr)
}

func (graph *GoGraph) writeLog(bytes []byte) {
	size, err := graph.log.Seek(0, 2)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Make sure a message doesn't cross chunk boundaries
	if (size + int64(len(bytes)) + 1) / bufsiz != size / bufsiz {
		pad := bufsiz - (size % bufsiz)
		fmt.Printf("Reached chunk boundary, padding from %d to %d\n", size, size + pad)
		padbytes := make([]byte, pad)
		padbytes[0] = 0xFF // Chunk terminator
		_, err := graph.log.Write(padbytes)
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	_, err = graph.log.Write(bytes)
	if err != nil {
		fmt.Println(err)
	}

	err = graph.log.Sync()
	if err != nil {
		fmt.Println(err)
	}
}

func (graph *GoGraph) Update(update GraphSetUpdate) error {
	graph.wsmutex.Lock()
	defer graph.wsmutex.Unlock()

	str, err := json.Marshal(update)
	if err != nil {
		return err
	}

	str = append(str, byte('\n'))
	graph.zipper.Write(str)
	graph.zipper.Flush()

	bytes := graph.zipbuf.Bytes()

	if graph.log != nil {
		graph.writeLog(bytes)
	}

	for i, conn := range graph.listeners {
		if (conn == nil) {
			continue
		}

		err := conn.WriteMessage(websocket.BinaryMessage, bytes)
		if err != nil {
			fmt.Println(err)
			graph.listeners[i] = nil
		}
	}

	graph.zipbuf.Reset();

	return nil
}
